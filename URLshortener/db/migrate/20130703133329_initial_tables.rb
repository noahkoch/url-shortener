class InitialTables < ActiveRecord::Migration
  def change
    create_table :long_urls do |t|
      t.string :url
    end

    create_table :short_urls do |t|
      t.string :url
      t.integer :long_id
      t.integer :user_id
    end

    create_table :clicks do |t|
      t.integer :short_id
      t.integer :user_id
      t.timestamps
    end

    create_table :users do |t|
      t.string :name
      t.string :email
    end

    create_table :tags do |t|
      t.string :name
    end

    create_table :comments do |t|
      t.string :comment
      t.integer :user_id
      t.integer :short_id
    end

    create_table :short_tag do |t|
      t.integer :short_id
      t.integer :tag_id
    end

  end
end
