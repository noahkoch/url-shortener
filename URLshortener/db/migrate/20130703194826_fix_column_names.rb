class FixColumnNames < ActiveRecord::Migration
  def up
    rename_column :clicks, :short_id, :short_url_id
    rename_column :comments, :short_id, :short_url_id
    rename_column :short_tags, :short_id, :short_url_id
    rename_column :short_urls, :long_id, :long_url_id
  end

  def down
  end
end
