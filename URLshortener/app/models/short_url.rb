require 'SecureRandom'

class ShortUrl < ActiveRecord::Base
  attr_accessible :url, :long_url_id, :user_id
  has_many :clicks
  belongs_to :long_url
  belongs_to :user

  def self.shorten_url(long, user)
    long_object = LongUrl.find_by_url(long) || LongUrl.create(:url => long)
    short_url = SecureRandom.urlsafe_base64(4)
    user_id = user.id
    p long_object
    long_url_id = long_object.id
    ShortUrl.create(:url => short_url, :long_url_id => long_url_id, :user_id => user_id)
  end
end