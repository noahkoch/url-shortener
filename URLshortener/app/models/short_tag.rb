class ShortTag < ActiveRecord::Base
  attr_accessible :short_url_id, :tag_id
  belongs_to :short_url
  belongs_to :tag
end