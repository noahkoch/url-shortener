class User < ActiveRecord::Base
  attr_accessible :name, :email
  has_many :clicks
  validates :email, :presence => true, :email => true, :uniqueness => true
  validates :name, :presence => true
end