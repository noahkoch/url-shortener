class LongUrl < ActiveRecord::Base

  attr_accessible :url

  validates :url, :presence => true, :uniqueness => true
  #:url => true,
end