class Comment < ActiveRecord::Base
  attr_accessible :short_url_id, :user_id, :comment
  belongs_to :short_url
  belongs_to :user
end