require 'uri'

class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute_name, value)
    unless record =~ URI::regexp
      record.errors[attribute_name] << (options[:message] || "is not a url")
    end
  end
end