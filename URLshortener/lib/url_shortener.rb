class URLShortener

  attr_accessor :user


  def create_account(name, email)
    @user = User.create(:name => name, :email => email)
  end

  def login(email)
    @user = User.find_by_email(email)
    #Returns nil if user doesn't already exist
  end

  def shorten_url(long_url)
    ShortUrl.shorten_url(long_url, @user)
  end

  def add_tag(short_url, tag)
    tag_object = Tag.find_by_name(tag) || Tag.create(:name => tag)
    short_object = ShortUrl.find_by_url(short_url)
    tag_id = tag_object.id
    short_id = short_object.id
    short_tag = ShortTag.where("short_url_id = ? AND tag_id = ?", short_id, tag_id)
    if short_tag.empty?
      ShortTag.create(:short_url_id => short_id, :tag_id => tag_id)
    end
  end

  def add_comment(comment, short_url)
    user_id = user.id
    short_object = ShortUrl.find_by_url(short_url)
    short_id = short_object.id
    unless short_object.nil?
      Comment.create(:comment => comment, :user_id => user_id, :short_url_id => short_id)
    end
  end

  def launch(short_url)
    short_object = ShortUrl.find_by_url(short_url)
    unless short_object.nil?
      long_id = short_object.long_url_id
      long_object = LongUrl.find_by_id(long_id)
      click(short_object.id)
      Launchy.open(long_object.url)
    end
  end

  def click(short_id)
    Click.create(:short_url_id => short_id, :user_id => user.id)
  end

  def count_clicks(short_id)
    clicks = (Click.where("short_url_id = ?", short_id)).length
  end

  def count_unique_clicks(short_id)
    Click.where("short_url_id = ?", short_id).group_by(&:user_id).count
  end

  def popular_per_tag

    clicks_per_short = Click.all.group_by(&:short_url_id)
    clicks_per_short.map {|key, value| clicks_per_short[key] = value.length}
    short_tags = ShortTag.all
    short_tags.each do |ob|
      test = clicks_per_short.dup
      if test[ob.short_url_id] == test.values.max
        p ob.short_url_id
      end
    end
  end

  def give_max(shorttags)
    max_clicks = shorttags.values.max
    shorttags.index(max_clicks)
  end
# SQL solution
  # select tags.id, count(clicks.id)
#   FROM short_urls
#   JOIN clicks on short_urls.id = clicks.short_url_id
#   JOIN short_tags ON clicks.short_url_id = short_tags.short_url_id
#   JOIN tags ON short_tags.tag_id = tags.id GROUP BY tags.name;

end